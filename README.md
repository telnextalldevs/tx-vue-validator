# README #

Libreria di validazione per Vue.js

### Istruzioni ###

```javascript
import { Validator } from 'tx-vue-validator'
Vue.use(Validator)

```

### Basic Usage ###

```html
<input name="myInput" v-validation="['required']" v-model="myVal" />
<span v-if="vvErrors.has('myVal')">{{ vvErrors.first('myVal') }}</span>
```
```javascript
save () {
  this.clearNotifications()

  if (this.vvErrors.any()) {
    this.$validator.showAll()
    return false
  } else {
    this.$validator.hideAll()
  }
}
```

### Group validation ###
In questo caso, quando si clicca salva, scatta solo la validazione degli input del grouppo group1

```html
<input name="myInput" v-validation="['required']" v-model="myVal" vv-group="group1" />
<input name="myInput2" v-validation="['required']" v-model="myVal2" vv-group="group2" />
<span v-if="vvErrors.has('myVal', 'group1')">{{ vvErrors.first('myVal', 'group1') }}</span>
```
```javascript
save () {
  this.clearNotifications()

  if (this.vvErrors.any('group1')) {
    this.$validator.showAll('group1')
    return false
  } else {
    this.$validator.hideAll('group1')
    goOn()
  }
}
```

### Conditional validations ###
```html
<input name="parentVal" v-model="parentVal" />
<input name="myInput" v-validation="[shouldApplyRequired() ? 'required' : '']" v-model="myVal" vv-group="group1" />
```
```javascript
shouldApplyRequired () {
  return this.parentVal === 'AAA'
}
```

### Available validations ###
```
required
email
daterange: params: {min: Timestamp, max: Timestamp} (la data deve essere tra x e y)
equal: params: Integer (il campo deve essere lungo esattamente x)
min: params: Integer (il campo è lungo almeno x)
max: params: Integer (il campo deve essere lungo al massimo x)
cf (codice fiscale valido)
piva (partita iva valida)
number (campo deve essere un numero)
regex: params: String
```

### Custom error messages ###
```html
<input 
  name="myInput" 
  v-validation="[{type: 'required', message: 'MANDATORY FIELD'}]" 
  v-model="myVal" 
/>
<span v-if="vvErrors.has('myVal')">{{ vvErrors.first('myVal') }}</span>
```

### Passing params ###
```html
<input name="myInput" v-validation="[{type: 'min', params: 5}]" v-model="myVal" />
<span v-if="vvErrors.has('myVal')">{{ vvErrors.first('myVal') }}</span>
```

### Component validations ###
```html
<my-cmp v-validation="['required']" v-model="xxx" />
```

My cmp
```html
<template>
  <div>
    My fancy input widget
    <input type="text" v-validation="['required']" v-model="innerVal" />
    <span v-if="vvErrors.has(id)">{{ vvErrors.first(id) }}</span>
  </div>
</template>
```
```javascript
export default {
  props: {
    id: {
      type: String,
      default () {
        return randomId()
      }
    },
    value: String
  },
  data () {
    return {
      innerValue: ''
    }
  },
  created () {
    this.innerValue = this.value
  },
  watch: {
    innerValue (newVal) {
      this.$emit('input', newVal)
    }
  }
}
```

### Multiple validations ###
```html
<input 
  name="myInput" 
  v-validation="[{type: 'min', params: 5}, 'required', 'number']" 
  v-model="myVal" 
/>
<span v-if="vvErrors.has('myVal')">{{ vvErrors.first('myVal') }}</span>
```