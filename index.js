import _ from 'lodash'
import isEmail from 'validator/lib/isEmail'
import cfpiva from 'cfpiva'
import Vue from 'vue'
import randomId from 'random-id'

var validatorsRegistry = {
  required: {
    validate: (rule, v) => v !== null && v !== '' && v !== undefined && (!rule.params.checkEmptyArray || !_.isArray(v) || !_.isEmpty(v)),
    getErrorMessage: (f, rule) => `Il campo "${f}" è obbligatorio`
  },
  email: {
    validate: (rule, v) => !v || isEmail(v),
    getErrorMessage: (f, rule) => `Il campo "${f}" non è una mail valida`
  },
  daterange: {
    validate: (rule, v) => !rule.params.min || !rule.params.max || rule.params.min < rule.params.max,
    getErrorMessage: (f, rule) => 'La data di inizio deve essere antecedente alla data di fine'
  },
  equal: {
    validate: (rule, v) => !rule.params || !v || v.length === rule.params,
    getErrorMessage: (f, rule) => `Il campo "${f}" deve essere lungo ${rule.params} caratteri`
  },
  min: {
    validate: (rule, v) => !rule.params || !v || v.length >= rule.params,
    getErrorMessage: (f, rule) => `Il campo "${f}" deve essere lungo almeno ${rule.params} caratteri`
  },
  max: {
    validate: (rule, v) => !rule.params || !v || v.length <= rule.params,
    getErrorMessage: (f, rule) => `Il campo "${f}" deve essere lungo al massimo ${rule.params} caratteri`
  },
  cf: {
    validate: (rule, v) => !v || cfpiva.controllaCF(v),
    getErrorMessage: (f, rule) => 'Codice fiscale non valido'
  },
  piva: {
    validate: (rule, v) => !v || cfpiva.controllaPIVA(v),
    getErrorMessage: (f, rule) => 'Partita IVA non valida'
  },
  number: {
    validate: (rule, v) => !v || !isNaN(v),
    getErrorMessage: (f, rule) => `Il campo "${f}" deve contenere solo numeri`
  },
  regex: {
    validate: (rule, v) => {
      if (rule.params && v) {
        var re = new RegExp(rule.params)
        return re.test(v)
      }

      return false
    },
    getErrorMessage: (f, rule) => `Il campo "${f}" non è valido`
  },
  checked: {
    validate: (rule, v) => v === true,
    getErrorMessage: (f, rule) => `Il campo "${f}" deve essere un valore vero`
  }
}

const errorObj = {
  validationsEnabled: false,
  validationsGroupEnabled: {},
  errors: {},
  has (name, group = undefined) {
    return _.find(this.errors[name] || [], {group: group}) && (this.validationsEnabled || (group && this.validationsGroupEnabled[group]))
  },
  first (field, group = undefined) {
    var res = _.find(this.errors[field] || [], {group: group})
    return (res && res.message) || undefined
  },
  any (group = undefined) {
    /* eslint-disable no-unneeded-ternary */
    return _(this.errors)
      .values()
      .flatten()
      .filter(e => !group || e.group === group)
      .value().length ? true : false
  }
}

class Rule {
  constructor (rule, field, fieldLabel, fieldGroup) {
    this.type = _.isObject(rule) ? rule.type : rule
    this.enabled = true
    this.params = (_.isObject(rule) && rule.params) || {}
    this.field = field
    this.fieldLabel = fieldLabel
    this.fieldGroup = fieldGroup
    this.lastValue = undefined
    this.message = _.isObject(rule) && rule.message
  }

  setEnabled (flag) {
    this.enabled = flag
    if (!flag) {
      errorObj.errors[this.field] = _.filter(errorObj.errors[this.field], v => v.rule !== this.type)
    } else if (!this.lastValue) {
      errorObj.errors[this.field].splice(_.findIndex(validations[this.field], { type: this.type }), 0, {
        rule: this.type,
        message: this.getErrorMessage(),
        group: this.fieldGroup
      })
    }
  }

  isOk (rule, valueToCheck) {
    this.lastValue = validatorsRegistry[this.type] && validatorsRegistry[this.type].validate(rule, valueToCheck)
    return !this.enabled || !validatorsRegistry[this.type] || this.lastValue
  }

  getErrorMessage () {
    if (!this.message) {
      return validatorsRegistry[this.type] && validatorsRegistry[this.type].getErrorMessage(this.fieldLabel, this)
    } else if (_.isFunction(this.message)) {
      return this.message(this.fieldLabel, this)
    } else {
      return this.message
    }
  }
}

var validations = {}

const validator = {
  showAll (group = undefined) {
    if (!group) {
      errorObj.validationsEnabled = true
    } else {
      Vue.set(errorObj.validationsGroupEnabled, group, true)
    }
  },
  hideAll (group) {
    if (!group) {
      errorObj.validationsEnabled = false
    } else {
      Vue.set(errorObj.validationsGroupEnabled, group, false)
    }
  },
  has (validationName, field) {
    return _.find(validations[field], { type: validationName, enabled: true }) || false
  },
  setEnabled (validationName, field, flag) {
    var rule = _.find(validations[field], { type: validationName })
    if (rule && flag !== rule.enabled) {
      rule.setEnabled(flag)
    }
  },
  isEnabled (validationName, field) {
    var rule = _.find(validations[field], { type: validationName })
    return rule && rule.enabled
  }
}

const getVModelValue = function (vnode) {
  if (!vnode.tag) return null
  if (vnode.tag === 'input' || vnode.tag === 'textarea') return vnode.data.domProps.value
  if (vnode.data.model) return vnode.data.model.value
  if (vnode.componentOptions.propsData) return vnode.componentOptions.propsData.value

  console.error('NO VMODEL ON INPUT')
  console.log(vnode.componentOptions)
  return null
}

const validateElement = function (el, Vue, binding, vnode, oldVnode, force = false) {
  let currValue = getVModelValue(vnode)
  let oldValue = getVModelValue(oldVnode)

  if (!force && _.isEqual(binding.oldValue, binding.value) && currValue === oldValue) return

  if (!_.isArray(binding.value)) {
    return console.warn('you should pass an array')
  }

  let fieldValidations = binding.value
  let attrs = vnode.child ? vnode.child : vnode.data.attrs
  let fieldName = attrs.id || attrs.internalId || el.dataset.validationId
  let fieldLabel = attrs.label || attrs.name || el.dataset.validationId
  let fieldGroup = (vnode.child && vnode.child.vvGroup) || vnode.data.attrs['vv-group']

  let oldAttrs = oldVnode.child || oldVnode.data.attrs || {}
  let oldFieldName = oldAttrs.id || oldAttrs.internalId || el.dataset.validationId

  if (force || !_.isEqual(binding.oldValue, binding.value) || fieldName !== oldFieldName) {
    if (oldFieldName) {
      validations[oldFieldName] = []
      errorObj.errors[oldFieldName] = []
    }

    validations[fieldName] = _.map(fieldValidations, fv => new Rule(fv, fieldName, fieldLabel, fieldGroup))
  }

  Vue.set(errorObj.errors, fieldName, [])

  _(validations[fieldName])
    .filter(rule => !rule.isOk(rule, currValue))
    .each(rule => {
      // if (fieldGroup && errorObj.validationsGroupEnabled[fieldGroup] === undefined) {
      //  Vue.set(errorObj.validationsGroupEnabled, fieldGroup, false)
      // }

      errorObj.errors[fieldName].push({
        rule: rule.type,
        message: rule.getErrorMessage(),
        group: fieldGroup
      })
    })
}

const install = function (Vue, options) {
  Object.defineProperty(Vue.prototype, '$validator', {
    get () {
      return validator
    }
  })

  Vue.directive('validation', {
    bind (el, binding, vnode, oldVnode) {
      let attrs = vnode.child ? vnode.child : vnode.data.attrs
      let fieldName = attrs.id || attrs.internalId
      if (!fieldName) el.dataset.validationId = randomId(10)

      validateElement(el, Vue, binding, vnode, oldVnode, true)
    },
    update (el, binding, vnode, oldVnode) {
      validateElement(el, Vue, binding, vnode, oldVnode)
    },
    unbind (el, binding, vnode) {
      let attrs = vnode.child ? vnode.child : vnode.data.attrs
      let fieldName = attrs.id || attrs.internalId || el.dataset.validationId

      validations = _.omit(validations, fieldName)
      errorObj.errors = _.omit(errorObj.errors, fieldName)
    }
  })

  Vue.mixin({
    data () {
      return {
        vvErrors: errorObj
      }
    }
  })
}

export default { install }
